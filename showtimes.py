from flask import Flask
from werkzeug.exceptions import NotFound
import json
import os


app = Flask(__name__)

with open("{}/database/showtimes.json".format(os.getcwd()), "r") as f:
    showtimes = json.load(f)


@app.route("/", methods=['GET'])
def hello():
    return json.dumps({
        "uri": "/",
        "subresource_uris": {
            "showtimes": "/showtimes",
            "showtime": "/showtimes/<date>"
        }
    })


@app.route("/showtimes", methods=['GET'])
def showtimes_list():
    return json.dumps(showtimes)


@app.route("/showtimes/<date>", methods=['GET'])
def showtimes_record(date):
    if date not in showtimes:
        raise NotFound
    print(showtimes[date])
    return json.dumps(showtimes[date])

if __name__ == "__main__":
    app.run(port=5002, debug=True)
